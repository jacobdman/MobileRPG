import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import AdventureStart from '../views/AdventureStart';
import CharacterCreation from '../views/CharacterCreation/CharacterCreation';
import CharacterStats from '../views/CharacterStats';
import Tutorial from '../views/Tutorial';
import TileStyling from '../views/TileStyling';
import MainGame from '../views/MainGame';

export default createSwitchNavigator({
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  // Main: AdventureStart,
  Main: TileStyling,
  // Main: Tutorial,
  // Main: MainGame,
  CharacterCreation: CharacterCreation,
  CharacterStats: CharacterStats,
});
